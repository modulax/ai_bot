
// var accessToken = "1de5e4f8c3f6487494240551f299ea07"; OG ACTIVEPIPE DEMO VERSION
var accessToken = "eb9096edf8fe422cac8ab777da29c2f1"; //WITH WEBHOOKS DEV ACCESS TOKEN

 var baseUrl = "https://api.api.ai/v1/";+

$(document).ready(function()
{
        $("#response").append('<div class="name">ActiveBot</div><div class="botTalk">Hi there! How can I help you today? </div>');

        $("#input").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                send();
            }
        });

        $("#rec").click(function(event) {
            switchRecognition();
        });

        $("#input").focus();
});

        var recognition;

        function startRecognition() {
            recognition = new webkitSpeechRecognition();
            recognition.onstart = function(event) {
                updateRec();
            };
            recognition.onresult = function(event) {
                var text = "";
                for (var i = event.resultIndex; i < event.results.length; ++i) {
                    text += event.results[i][0].transcript;
                }
                setInput(text);
                stopRecognition();
            };
            recognition.onend = function() {
                stopRecognition();
            };
            recognition.lang = "en-US";
            recognition.start();
        }

        function stopRecognition() {
            if (recognition) {
                recognition.stop();
                recognition = null;
            }
            updateRec();
        }

        function switchRecognition() {
            if (recognition) {
                stopRecognition();
            } else {
                startRecognition();
            }
        }

        function setInput(text) {
            $("#input").val(text);
            send();


        }

        function updateRec() {
            // $("#rec").text(recognition ? "Stop" : "Speak");
            // $("#rec img").attr("src",recognition ? "images/mic_on.svg" : "images/mic.svg");
            if(recognition)
            {
                    $("#rec").addClass("recording");
            }else{
                $("#rec").removeClass("recording");
            }


        }

        function send() {
            var text = $("#input").val();
            $("#response").append('<div class="name">Me</div><div class="userTalk">' + text + '</div>');
            setTimeout(function()
            {
                 $("#response").append('<div id="loading"></div>');
            $('#response').scrollTop(500000000);
            },500);



            $.ajax({
                type: "POST",
                url: baseUrl + "query?v=20150910",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                data: JSON.stringify(
                    {
                        query: text,
                        lang: "en",
                        sessionId: "somerandomthing"
                    }),

                success: function(data) {
                    // setResponse(JSON.stringify(data, undefined, 2));
                    console.log(data);
                    $("#input").val('');
                    $("#loading").remove();
                    setResponse(data.result.fulfillment.speech);
                },
                error: function() {
                    $("#loading").remove();
                    setResponse("Internal Server Error");
                }
            });
            // setResponse("Loading...");
        }

        function setResponse(val) {
        	$("#loading").remove();
            $("#response").append('<div class="name">ActiveBot</div><div class="botTalk">' + val + '</div>');
            $('#response').scrollTop(500000000);

        }
