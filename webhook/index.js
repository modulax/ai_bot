const express = require('express')
const bodyParser = require('body-parser')
const request = require('request')
var $ = require('jquery')
const app = express()

app.use(bodyParser.json())
app.set('port', (process.env.PORT || 5000))

const REQUIRE_AUTH = true
const AUTH_TOKEN = 'an-example-token'

app.get('/', function (req, res) {
  res.send('Use the /webhook endpoint.')
})
app.get('/webhook', function (req, res) {
  res.send('You must POST your request')
})

app.post('/webhook', function (req, res) {

    // we expect to receive JSON data from api.ai here.
    // the payload is stored on req.body
    console.log("________ PAYLOAD FORM API ________");
    console.log(req.body)


    // we have a simple authentication
    if (REQUIRE_AUTH) {
      if (req.headers['auth-token'] !== AUTH_TOKEN) {
        return res.status(401).send('Unauthorized')
      }
    }

    // and some validation too
    if (!req.body || !req.body.result || !req.body.result.parameters) {
      return res.status(400).send('Bad Request')
    }

    // parameters are stored in req.body.result.parameters
    if(req.body.result.action == 'welcome.user'){

        var userName = req.body.result.parameters['given-name']
        var webhookReply = 'Hello ' + userName + '! WH.'

    }else if(req.body.result.action == 'color.user'){

        var webhookReply = "My favourite color is red!"

      // USING REQUEST
        var request = require('request');

    //     type: "GET",
    // url: 'http://api.activepipe.com/me',
    // data: {},
    // crossDomain: true,
    // dataType: 'json',
    // xhrFields: {
    //     withCredentials: true
    // },

var option = {
    url: 'http://api.activepipe.com.local/me',
    data: {},
    crossDomain: true,
    dataType: 'json',
    xhrFields: {
        withCredentials: true
    }
};
request(option,
    function (error, response, body) {
        console.log("");

        console.log(body);
    }
);


    }
    // the most basic response
    res.status(200).json({
      source: 'webhook',
      speech: webhookReply,
      displayText: webhookReply
    })

})



app.listen(app.get('port'), function () {
  console.log('* Webhook service is listening on port:' + app.get('port'))
})

/*---------------------------------------
    RESOURCES
-----------------------------------------*/
//git add .
//git commit -m ""
//git push heroku master
//https://github.com/supermamon/apiai-nodejs-webhook-sample
//ngrok http 5000
//https://dialogflow-activepipe.herokuapp.com/webhook
